#include "panel.h"
#include <iostream>

Panel::Panel(QWidget *parent) :
    QWidget(parent)
{
    //Init XCB subsystem
    _xcbHandler = new XCBHandler();

    //Init taskbar logic
    _taskbarLogic = new TaskbarLogic(_xcbHandler, winId());
    connect(_taskbarLogic, SIGNAL(OnActiveWindow(xcb_window_t)), this, SLOT(OnActiveWindow(xcb_window_t)));
    connect(_taskbarLogic, SIGNAL(OnNewWindow()), this, SLOT(OnNewWindow()));
    connect(
        _taskbarLogic, SIGNAL(OnWindowName(xcb_window_t,QString)),
        this, SLOT(OnWindowName(xcb_window_t,QString)));

    //Init event timer
    _timer.setInterval(1);
    connect(&_timer, SIGNAL(timeout()), _taskbarLogic, SLOT(ProcessEvents()));
    _timer.start();

    //Setup layout
    _layout = new QHBoxLayout();
    _layout->setMargin(2);
    setLayout(_layout);

    QSpacerItem *spacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);
    _layout->addSpacerItem(spacer);

    //Setup panel
    resize(QApplication::desktop()->screenGeometry().width(), 24);
    move(0,0);

    //Setup button group
    _buttonGroup = new QButtonGroup();

    //Get all windows
    GetAllWindows();
}

void Panel::OnWindowName(xcb_window_t window, const QString &name)
{
    if(_windows.contains(window))
    {
        _windows[window]->setText(name);
    }
}

void Panel::GetAllWindows()
{
    QVector<xcb_window_t> values = _taskbarLogic->GetVisibleWindows();

    //Remove old windows
    QMap<xcb_window_t, WindowHandler*>::iterator iterator = _windows.begin();
    while(iterator != _windows.end())
    {
        if(!values.contains(iterator.key()))
        {
            delete iterator.value();
            iterator = _windows.erase(iterator);
        }
        else
        {
            ++iterator;
        }
    }

    //Add new windows
    for(int i = 0; i < values.count(); i++)
    {
        xcb_window_t currentWindow = values[i];

        if(!_windows.contains(currentWindow))
        {
            WindowHandler *windowHandler = new WindowHandler(_taskbarLogic, currentWindow);

            _xcbHandler->SetEventMask(currentWindow, XCB_EVENT_MASK_PROPERTY_CHANGE);

            _buttonGroup->addButton(windowHandler);
            _layout->insertWidget(0, windowHandler);

            _windows.insert(currentWindow, windowHandler);
        }
    }
}

void Panel::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    _taskbarLogic->SetDockMode(true, this);
}

Panel::~Panel()
{
    delete _buttonGroup;
    delete _xcbHandler;
    delete _taskbarLogic;
}

void Panel::OnActiveWindow(xcb_window_t window)
{
    if(_windows.contains(window))
    {
        _windows[window]->WindowActivated();
    }
}

void Panel::OnNewWindow()
{
    GetAllWindows();
}

#include "xcbhandler.h"
#include <iostream>

using namespace std;

XCBHandler::XCBHandler()
{
    _connection = xcb_connect(NULL, NULL);
    if(xcb_connection_has_error(_connection))
    {
        std::cerr << "Error: xcb_connect!" << std::endl;
        exit(1);
    }

    _screen = xcb_setup_roots_iterator( xcb_get_setup(_connection) ).data;
    _root = _screen->root;
}

XCBHandler::~XCBHandler()
{
    xcb_disconnect(_connection);
}

void XCBHandler::Test()
{

}

xcb_window_t XCBHandler::RootWindow() const
{
    return _root;
}

xcb_connection_t *XCBHandler::Connection() const
{
    return _connection;
}

void XCBHandler::SetEventMask(xcb_window_t window, uint32_t value)
{
    uint32_t mask = XCB_CW_EVENT_MASK;
    uint32_t values[2];
    xcb_void_cookie_t cookie;

    values[0] = value;

    cookie = xcb_change_window_attributes_checked(_connection, window, mask, values);
    xcb_request_check(_connection, cookie);
    xcb_flush(_connection);
}

xcb_atom_t XCBHandler::GetAtom(const QString& atomName)
{
    xcb_atom_t atom;
    xcb_intern_atom_cookie_t cookie;
    xcb_intern_atom_reply_t *reply;

    if(_cachedAtoms.contains(atomName))
    {
        return _cachedAtoms[atomName];
    }

    cookie = xcb_intern_atom(_connection, 1, atomName.length(), atomName.toStdString().c_str());
    reply = xcb_intern_atom_reply(_connection, cookie, NULL);
    if(reply != NULL)
    {
        atom = reply->atom;
        free(reply);
    }
    if(atom == XCB_ATOM_NONE)
    {
        std::cerr << "Error: Atom not found: " + atomName.toStdString() << std::endl;
    }
    else
    {
        _cachedAtoms.insert(atomName, atom);
    }

    return atom;
}

void XCBHandler::ChangeWindowProperty(xcb_window_t window, const QString &property, const QString &propertyType, int dataFormat, void *data, int itemsCount)
{
    xcb_void_cookie_t cookie;
    xcb_generic_error_t *error;

    cookie = xcb_change_property_checked(
                _connection,
                XCB_PROP_MODE_REPLACE,
                window,
                GetAtom(property),
                GetAtom(propertyType),
                dataFormat,
                itemsCount,
                data);

    error = xcb_request_check(_connection, cookie);

    if(error != NULL)
    {
        std::cerr << "Error: ChangeWindowProperty: " << property.toStdString() << std::endl;
        delete error;
    }

    xcb_flush(_connection);
}

void XCBHandler::ChangeWindowPropertyCardinal(xcb_window_t window, const QString &property, uint32_t *data, int itemsCount)
{
    ChangeWindowProperty(window, property, "CARDINAL", 32, data, itemsCount);
}

void XCBHandler::ChangeWindowPropertyAtom(xcb_window_t window, const QString &property, const QVector<QString> &atomNames)
{
    int atomsCount = atomNames.count();
    xcb_atom_t *atoms = new xcb_atom_t[atomsCount];
    for(int i = 0; i < atomsCount; i++)
    {
        atoms[i] = GetAtom(atomNames[i]);
    }
    ChangeWindowProperty(window, property, "ATOM", 32, atoms, atomsCount);
    delete[] atoms;
}

void XCBHandler::ChangeWindowStackMode(xcb_window_t window, uint32_t stackMode)
{
    xcb_configure_window(_connection, window, XCB_CONFIG_WINDOW_STACK_MODE, &stackMode);
}

void XCBHandler::SendClientMessage(xcb_window_t window, const char *eventType, xcb_client_message_data_t data)
{
    xcb_client_message_event_t *event = new xcb_client_message_event_t;

    event->sequence = 0;
    event->response_type = XCB_CLIENT_MESSAGE;
    event->window = window;
    event->format = 32;
    event->type = GetAtom(eventType);
    event->data = data;

    xcb_send_event(
        _connection,
        false,
        _root,
        XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT | XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY,
        reinterpret_cast<const char *>(event));

    xcb_flush(_connection);
    delete event;
}

void *XCBHandler::GetWindowProperty(xcb_window_t window, const QString &property, const QString &propertyType, int *length)
{
    xcb_get_property_cookie_t cookie;
    xcb_get_property_reply_t *reply;
    void *data = NULL;

    cookie = xcb_get_property(_connection, 0, window, GetAtom(property), GetAtom(propertyType), 0, 128);
    reply = xcb_get_property_reply(_connection, cookie, NULL);
    if(reply != NULL)
    {
        *length = xcb_get_property_value_length(reply);
        if(*length > 0)
        {
            data = malloc(*length);
            memcpy(data, xcb_get_property_value(reply), *length);
        }
        free(reply);
    }
    return data;
}

QVector<uint32_t> XCBHandler::GetWindowPropertyArray(xcb_window_t window, const QString &property, const QString &propertyType)
{
    xcb_get_property_cookie_t cookie;
    xcb_get_property_reply_t *reply;
    QVector<uint32_t> properties;

    cookie = xcb_get_property(_connection, 0, window, GetAtom(property), GetAtom(propertyType), 0, 128);
    reply = xcb_get_property_reply(_connection, cookie, NULL);
    if(reply != NULL)
    {
        int length = xcb_get_property_value_length(reply);
        if(length > 0)
        {
            int propertiesCount = length / 4;
            uint32_t *data = static_cast<uint32_t*>(xcb_get_property_value(reply));

            for(int i = 0; i < propertiesCount; i++)
            {
                properties.append(data[i]);
            }
        }
        free(reply);
    }
    return properties;
}

xcb_window_t XCBHandler::GetWindowPropertyWindow(xcb_window_t window, const QString &name)
{
    void *data;
    int dataSize;
    xcb_window_t result = 0;

    data = GetWindowProperty(window, name, "WINDOW", &dataSize);

    if(data != NULL)
    {
        memcpy(&result, data, dataSize);
        free(data);
    }
    return result;
}



void XCBHandler::DeleteWindowProperty(xcb_window_t window, const QString &propertyName)
{
    xcb_delete_property(_connection, window, GetAtom(propertyName));
    xcb_flush(_connection);
}

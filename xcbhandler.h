#ifndef XCBHANDLER_H
#define XCBHANDLER_H

#include <xcb/xcb.h>
#include <QString>
#include <QVector>
#include <QMap>

class XCBHandler
{
public:
    XCBHandler();
    ~XCBHandler();

    //test
    void Test();

    //Getters
    xcb_window_t RootWindow() const;
    xcb_connection_t *Connection() const;

    //Helpers
    xcb_atom_t GetAtom(const QString& atomName);
    QVector<uint32_t> GetWindowPropertyArray(xcb_window_t window, const QString &property, const QString &propertyType);
    void *GetWindowProperty(xcb_window_t window, const QString &property, const QString &propertyType, int *length);
    xcb_window_t GetWindowPropertyWindow(xcb_window_t window, const QString& name);

    void DeleteWindowProperty(xcb_window_t window, const QString& propertyName);

    void SetEventMask(xcb_window_t window, uint32_t value);
    void ChangeWindowPropertyCardinal(xcb_window_t window, const QString &property, uint32_t *data, int itemsCount);
    void ChangeWindowPropertyAtom(xcb_window_t window, const QString &property, const QVector<QString> &atomNames);
    void ChangeWindowStackMode(xcb_window_t window, uint32_t stackMode);

    void SendClientMessage(xcb_window_t window, const char *eventType, xcb_client_message_data_t data);

private:
    void ChangeWindowProperty(xcb_window_t window, const QString &property, const QString &propertyType, int dataFormat, void *data, int itemsCount);

    xcb_connection_t *_connection;
    xcb_screen_t *_screen;
    xcb_window_t _root;

    QMap<QString, xcb_atom_t> _cachedAtoms;
};

#endif // XCBHANDLER_H

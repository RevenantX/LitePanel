#-------------------------------------------------
#
# Project created by QtCreator 2013-09-03T12:36:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = litepanel
TEMPLATE = app

SOURCES += main.cpp\
        panel.cpp \
    xcbhandler.cpp \
    windowhandler.cpp \
    taskbarlogic.cpp

HEADERS  += panel.h \
    xcbhandler.h \
    windowhandler.h \
    taskbarlogic.h

FORMS    +=

LIBS += -lxcb

#ifndef PANEL_H
#define PANEL_H

#include <QWidget>
#include <QTimer>
#include <QResizeEvent>
#include <QHBoxLayout>
#include <QPushButton>
#include <QApplication>
#include <QDesktopWidget>
#include <QButtonGroup>
#include <QSpacerItem>

#include "xcbhandler.h"
#include "windowhandler.h"
#include "taskbarlogic.h"

namespace Ui {
class Panel;
}

class Panel : public QWidget
{
    Q_OBJECT
    
public:
    explicit Panel(QWidget *parent = 0);
    ~Panel();

private slots:
    void OnActiveWindow(xcb_window_t window);
    void OnNewWindow();
    void OnWindowName(xcb_window_t window, const QString &name);

protected:
    virtual void resizeEvent( QResizeEvent *event );

private:
    void GetAllWindows();

    QHBoxLayout *_layout;

    QButtonGroup *_buttonGroup;
    QTimer _timer;
    QMap<xcb_window_t, WindowHandler*> _windows;

    XCBHandler *_xcbHandler;
    TaskbarLogic *_taskbarLogic;
};

#endif // PANEL_H

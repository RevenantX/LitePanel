#ifndef WINDOWHANDLER_H
#define WINDOWHANDLER_H

#include "taskbarlogic.h"
#include <QWidget>
#include <QPushButton>

class WindowHandler : public QPushButton
{
    Q_OBJECT
public:
    WindowHandler(TaskbarLogic *taskbarLogic, xcb_window_t window);
    ~WindowHandler();

    xcb_window_t Window();
    void WindowActivated();

public slots:
    void Activate();

private:
    xcb_window_t _window;
    TaskbarLogic *_taskbarLogic;
};

#endif // WINDOWHANDLER_H

#ifndef TASKBARLOGIC_H
#define TASKBARLOGIC_H

#include "xcbhandler.h"
#include <QVector>
#include <QWidget>
#include <QObject>

class TaskbarLogic : public QObject
{
    Q_OBJECT
public:
    TaskbarLogic(XCBHandler *xcbHandler, xcb_window_t ourWindow);

    xcb_window_t GetLastActiveWindow();

    bool IsNormalWindow(xcb_window_t window);

    void SetDockMode(bool enable, QWidget *dockWidget);
    void ActivateWindow(xcb_window_t window);
    void MinimizeWindow(xcb_window_t window);
    xcb_window_t GetActiveWindow();
    QVector<xcb_window_t> GetVisibleWindows();
    QString GetWindowName(xcb_window_t window);

public slots:
    void ProcessEvents();

signals:
    void OnWindowName(xcb_window_t window, const QString &name);
    void OnActiveWindow(xcb_window_t window);
    void OnNewWindow();

private:
    XCBHandler *_xcbHandler;
    xcb_window_t _activeWindow;
    xcb_window_t _ourWindow;
};

#endif // TASKBARLOGIC_H

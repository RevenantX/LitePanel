#include "taskbarlogic.h"
#include <iostream>

TaskbarLogic::TaskbarLogic(XCBHandler *xcbHandler, xcb_window_t ourWindow) :
    _xcbHandler(xcbHandler),
    _ourWindow(ourWindow)
{
    _xcbHandler->SetEventMask(_xcbHandler->RootWindow(), XCB_EVENT_MASK_PROPERTY_CHANGE);
}

xcb_window_t TaskbarLogic::GetLastActiveWindow()
{
    return _activeWindow;
}

bool TaskbarLogic::IsNormalWindow(xcb_window_t window)
{
    QVector<uint32_t> props = _xcbHandler->GetWindowPropertyArray(window, "_NET_WM_WINDOW_TYPE", "ATOM");
    QVector<uint32_t> windowStates = _xcbHandler->GetWindowPropertyArray(window, "_NET_WM_STATE", "ATOM");

    bool visible = props.size() == 0 || (props.count() == 1 && props[0] == _xcbHandler->GetAtom("_NET_WM_WINDOW_TYPE_NORMAL"));

    if(windowStates.contains(_xcbHandler->GetAtom("_NET_WM_STATE_SKIP_TASKBAR")))
    {
        visible = false;
    }

    return visible;
}

//data[0] LEFT
//data[1] RIGHT
//data[2] TOP
//data[3] BOTTOM
//data[4] LeftStartY
//data[5] LeftEndY
//data[6] RightStartY
//data[7] RightEndY
//data[8] TopStartX
//data[9] TopEndX
//data[10]BottomStartX
//data[11]BottomEndX

void TaskbarLogic::SetDockMode(bool enable, QWidget *dockWidget)
{
    xcb_window_t window = dockWidget->winId();
    QVector<QString> atomNames;

    //Set dock mode
    //uint32_t dock = enable ? 0xFFFFFFFF : 0;
    //_xcbHandler->ChangeWindowPropertyCardinal(window, "_NET_WM_WINDOW_TYPE_DOCK", &dock, 1);

    //Set space reserving
    if(enable)
    {
        uint32_t data[12] = {0};

        data[2] = dockWidget->y() + dockWidget->height();   //TOP
        data[8] = dockWidget->x();                          //TopStartX
        data[9] = dockWidget->x() + dockWidget->width();    //TopEndX

        _xcbHandler->ChangeWindowPropertyCardinal(window, "_NET_WM_STRUT_PARTIAL", data, 12);
        _xcbHandler->ChangeWindowPropertyCardinal(window, "_NET_WM_STRUT", data, 4);

        atomNames.push_back("_NET_WM_WINDOW_TYPE_DOCK");
    }
    else
    {
        _xcbHandler->DeleteWindowProperty(window, "_NET_WM_STRUT_PARTIAL");
        _xcbHandler->DeleteWindowProperty(window, "_NET_WM_STRUT");

        atomNames.push_back("_NET_WM_WINDOW_TYPE_NORMAL");
    }

    _xcbHandler->ChangeWindowPropertyAtom(window, "_NET_WM_WINDOW_TYPE", atomNames);
}

QString TaskbarLogic::GetWindowName(xcb_window_t window)
{
    QString nameString;
    void *wp;
    int outLength;

    if( (wp = _xcbHandler->GetWindowProperty(window, "_NET_WM_VISIBLE_NAME", "UTF8_STRING", &outLength)) ||
        (wp = _xcbHandler->GetWindowProperty(window, "_NET_WM_NAME", "UTF8_STRING", &outLength)) )
    {
        nameString = QString::fromUtf8((const char *)wp, outLength);
        free(wp);
    }
    else if( (wp = _xcbHandler->GetWindowProperty(window, "WM_NAME", "STRING", &outLength)) )
    {
        nameString = QString((const char *)wp);
        free(wp);
    }

    return nameString;
}

void TaskbarLogic::ProcessEvents()
{
    xcb_generic_event_t *event;
    xcb_property_notify_event_t *propertyNotify;

    while( (event = xcb_poll_for_event(_xcbHandler->Connection())) )
    {
        switch(event->response_type)
        {
            case XCB_PROPERTY_NOTIFY:
            {
                propertyNotify = reinterpret_cast<xcb_property_notify_event_t*>(event);

                if(propertyNotify->window == _xcbHandler->RootWindow())
                {
                    if(propertyNotify->atom == _xcbHandler->GetAtom("_NET_CLIENT_LIST"))
                    {
                        emit OnNewWindow();
                        //std::cout << "_NET_CLIENT_LIST NOTIFY!" << std::endl;
                    }

                    if(propertyNotify->atom == _xcbHandler->GetAtom("_NET_ACTIVE_WINDOW"))
                    {
                        xcb_window_t window = _xcbHandler->GetWindowPropertyWindow(propertyNotify->window, "_NET_ACTIVE_WINDOW");

                        if(IsNormalWindow(window) && window != _ourWindow)
                        {
                            _activeWindow = window;
                        }

                        emit OnActiveWindow(window);
                    }
                }
                else
                {
                    if(propertyNotify->atom == _xcbHandler->GetAtom("WM_NAME"))
                    {
                        emit OnWindowName(propertyNotify->window, GetWindowName(propertyNotify->window));
                    }
                }
            }
            break;
        }

        free(event);
    }
}

void TaskbarLogic::ActivateWindow(xcb_window_t window)
{
    xcb_client_message_data_t data;
    memset(&data, 0, sizeof(data));
    data.data32[0] = 2;
    data.data32[1] = 0;
    data.data32[2] = 0;

    _xcbHandler->SendClientMessage(window, "_NET_ACTIVE_WINDOW", data);
    _activeWindow = window;
}

void TaskbarLogic::MinimizeWindow(xcb_window_t window)
{
    xcb_client_message_data_t data;
    memset(&data, 0, sizeof(data));
    data.data32[0] = 3;

    _xcbHandler->SendClientMessage(window, "WM_CHANGE_STATE", data);

    if(window == _activeWindow)
    {
        _activeWindow = 0;
    }
}

xcb_window_t TaskbarLogic::GetActiveWindow()
{
    return _xcbHandler->GetWindowPropertyWindow(_xcbHandler->RootWindow(), "_NET_ACTIVE_WINDOW");
}

QVector<xcb_window_t> TaskbarLogic::GetVisibleWindows()
{
    QVector<uint32_t> values = _xcbHandler->GetWindowPropertyArray(_xcbHandler->RootWindow(), "_NET_CLIENT_LIST", "WINDOW");
    QVector<xcb_window_t> visibleWindows;

    for(int i = 0; i < values.count(); i++)
    {
        xcb_window_t currentWindow = values[i];

        if(IsNormalWindow(currentWindow))
        {
            visibleWindows.append(currentWindow);
        }
    }

    return visibleWindows;
}

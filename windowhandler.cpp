#include "windowhandler.h"

WindowHandler::WindowHandler(TaskbarLogic *taskbarLogic, xcb_window_t window) :
    _window(window),
    _taskbarLogic(taskbarLogic)
{
    setText(_taskbarLogic->GetWindowName(_window));
    connect(this, SIGNAL(clicked()), this, SLOT(Activate()));
}

WindowHandler::~WindowHandler()
{

}

void WindowHandler::Activate()
{
    if(_taskbarLogic->GetLastActiveWindow() == _window)
    {
        setCheckable(false);
        setChecked(false);
        update();
        _taskbarLogic->MinimizeWindow(_window);
    }
    else
    {
        setCheckable(true);
        setChecked(true);
        _taskbarLogic->ActivateWindow(_window);
    }
}

void WindowHandler::WindowActivated()
{
    setCheckable(true);
    setChecked(true);
}
